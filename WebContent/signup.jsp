<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="./CSS/style.css" rel="stylesheet" type="text/css">
	<title>ユーザー新規登録</title>
	</head>
	<body>
	<div class="header">
		<ul>
			<li class="name"><span class="hello">こんにちは　</span><c:out value="${loginUser.name}"/><span class="hello">　さん</span></li>
			<li><a href="./">トップ</a></li>
			<li><a href="manageUser">ユーザー編集画面に戻る</a></li>
		</ul>
		<h1 class="h1">ユーザー新規登録</h1>
	</div>

		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		<div class="signup">
			<form action="signup" method="post" class="form"><br />

				<div class="form">		ログインID		<br/><input name="loginId" value="${loginId}" id="loginId" class="inputForm" placeholder="半角英数字で6～20文字" /></div>
				<div class="form">		パスワード		<br/><input name="password" type="password" id="password" class="inputForm" placeholder="半角英数字と記号で6～20文字"/></div>
				<div class="form">		パスワード確認	<br/><input type="password" name="checkPassword" id="checkPassword" class="inputForm" placeholder="上の新しいパスワードを再入力"/></div>
				<div class="form">		名前			<br/><input name="name" value="${name}" id="name" class="inputForm" placeholder="1～10文字"/></div>
				<div class="form">	部署		<br/>
					<select name="branchId" class="inputForm">
						<c:forEach items="${branches}" var="branch">
							<c:if test="${prevInputBranch != branch.id}">
								<option value="${branch.id}">
								<c:out value="${branch.name}"/>
								</option>
							</c:if>
							<c:if test="${prevInputBranch == branch.id}">
								<option selected value="${branch.id}">
								<c:out value="${branch.name}"/>
								</option>
							</c:if>
						</c:forEach>
					</select>
				</div>

				<div class="form">	役職		<br/>
					<select name="positionId" class="inputForm">
						<c:forEach items="${positions}" var="position">
							<c:if test="${prevInputPosition != position.id}">
								<option value="${position.id}">
								<c:out value="${position.name}"/>
								</option>
							</c:if>
							<c:if test="${prevInputPosition == position.id}">
								<option selected value="${position.id}">
								<c:out value="${position.name}"/>
								</option>
							</c:if>
						</c:forEach>
					</select>
				</div><br/>
				<div class="center"><input type="submit" value="新規登録" class="btn-simple"/></div><br/>
			</form>
			<div class="copyright">Copyright(c)OchiaiShun</div>
		</div>
		</div>
	</body>
</html>