<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./CSS/style.css" rel="stylesheet" type="text/css">
		<title>ログイン</title>
	</head>
	<body>

	<div class="login-header">
		<h1 class="h1">ログイン</h1>
	</div>


		<div class="login-main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<form action="login" method="post" class="form"><br />
				<div class="form">ログインID<br/><input name="loginId" value="${loginId}" id="loginId" class="inputForm"/></div><br />
				<div class="form">パスワード<br/><input name="password" type="password" id="password" class="inputForm"/></div><br />
				<div class="center"><input type="submit" value="ログイン" class="btn-simple" /></div><br />
			</form>

			<div class="copyright"> Copyright(c)OchiaiShun</div>
		</div>
	</body>
</html>