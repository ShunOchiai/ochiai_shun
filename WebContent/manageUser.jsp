<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
		<link href="./CSS/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
		function deleteCheck(name){
			if(window.confirm('アカウント「'+name+'」を停止します\n停止されたユーザーはログインができなくなり、掲示板の投稿や閲覧ができなくなります\n停止してもよろしいですか？')){
				window.alert('アカウントを停止しました');
				return true;

			}else{
				window.alert('キャンセルしました');
				return false;
			}
		}

		function rebornCheck(name){
			if(window.confirm('アカウント「'+name +'」の停止を解除します\n解除しても問題ない素行のユーザーかどうかを今一度確認してください\n解除してもよろしいですか？')){
				window.alert('アカウントの停止を解除しました');
				return true;

			}else{
				window.alert('キャンセルしました');
				return false;
			}
		}
		</script>
	</head>
	<body>

		<div class="header">
			<ul>
				<li class="name"><span class="hello">こんにちは　</span><c:out value="${loginUser.name}"/><span class="hello">　さん</span></li>
				<li><a href="./">トップに戻る</a></li>
				<li><a href="signup">ユーザー新規登録</a></li>
			</ul>
			<h1 class="h1">ユーザー管理画面</h1>
		</div>

		<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>

		<table class="userInfo">
			<tr><th width="25%">名前</th><th width="10%">支店名</th><th width="20%">役職名</th><th width="25%">アカウント状態</th><th width="20%">ユーザー編集</th></tr>
				<c:forEach items="${users}" var="user">
					<c:if test="${user.isDeleted == 0}"><tr></c:if>
					<c:if test="${user.isDeleted == 1}"><tr class="isDeleted"></c:if>
					<td><c:out value="${user.name}" /></td>
						<td><c:out value="${user.branchName}" /></td>
						<td><c:out value="${user.positionName}" /></td>
						<td>
							<c:if test="${user.id == loginUser.id}">
								ログイン中です
							</c:if>
							<c:if test="${user.id != loginUser.id}">

								<c:if test="${user.isDeleted == 0}">
									<form action="toggleIsDeleted" method="get" onSubmit="return deleteCheck('${user.name}')">
										<input type="hidden" name="isDeleted" value="${user.id}">
											稼働中
										<input type="submit" class="btn-simple-mini" value="停止">
									</form>
								</c:if>

								<c:if test="${user.isDeleted == 1}">
									<form action="toggleIsDeleted" method="get" onSubmit="return rebornCheck('${user.name}')">
										<input type="hidden" name="isDeleted" value="${user.id}">
										停止中
										<input type="submit" class="btn-simple-mini" value="再稼動">
									</form>
								</c:if>

							</c:if>
						</td>
						<td><a href="editUser?userId=${user.id}">編集画面へ</a><br/></td>
					</tr>

				</c:forEach>
		</table>
		<div class="copyright">Copyright(c)OchiaiShun</div>
		</div>
	</body>
</html>