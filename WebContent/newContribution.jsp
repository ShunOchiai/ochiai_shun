<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./CSS/style.css" rel="stylesheet" type="text/css">
		<title>新規投稿</title>
	</head>
	<body>

		<div class="header">
			<ul>
				<li class="name"><span class="hello">こんにちは　</span><c:out value="${loginUser.name}"/><span class="hello">　さん</span></li>
				<li><a href="./">トップに戻る</a></li>
			</ul>
			<h1 class="h1">新規投稿</h1>
		</div>

		<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>

			<div class="newContribution">
				<form action="newContribution" method="post">
				<br />
				<div class="form">タイトル　<span class="mini-gray">30文字以内</span><br/><input name="title" value="${title}" id="title" class="inputForm"/></div><br/>
				<div class="form">カテゴリー　<span class="mini-gray">10文字以内</span><input name="category" value="${category}" id="category" class="inputForm"/></div><br/>
				<div class="form">本文　<span class="mini-gray">1000文字以内（改行は2文字として扱われます）</span><textarea name="contribution" maxlength="1000" class="contribution-box" style="resize: none;" >${contribution}</textarea></div>
				<input type="submit" value="投稿する" class="btn-simple">
				</form>
			</div>
		<div class="copyright"> Copyright(c)OchiaiShun</div>
		</div>
	</body>
</html>
