<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./CSS/style.css" rel="stylesheet" type="text/css">
		<title>ユーザー編集</title>
	</head>
	<body>

	<div class="header">
		<ul>
			<li class="name"><span class="hello">こんにちは　</span><c:out value="${loginUser.name}"/><span class="hello">　さん</span></li>
			<li><a href="./">トップ</a></li>
			<li><a href="manageUser">ユーザー編集画面に戻る</a></li>
		</ul>
		<h1 class="h1">ユーザー編集</h1>
	</div>


	<div class="main-contents">
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

		<form action="editUser" method="post"><br/>
			<input type="hidden" name="userId" value="${user.id}">
			<div class="form">		ログインID		<span class="mini-gray">半角英数字で6～20文字</span><input name="loginId" value="${user.loginId}" id="loginId" class="inputForm"/></div><br/>
			<div class="form">		パスワード		<span class="mini-gray">半角英数字と記号で6～20文字</span><input type="password" name="password" id="password" class="inputForm" placeholder="現在のパスワードから変更しない場合は入力しないでください"/></div><br/>
			<div class="form">		パスワード確認	<span class="mini-gray">上記のパスワードの再入力</span><input type="password" name="checkPassword" id="checkPassword" class="inputForm" placeholder="現在のパスワードから変更しない場合は入力しないでください"/></div><br/>
			<div class="form">		名前			<span class="mini-gray">1～10文字</span><input name="name" value="${user.name}" id="name" class="inputForm"/></div><br/>

			<c:if test="${user.id != loginUser.id}">
				<div class="form">			部署			<br/>
				<select name="branchId" class="inputForm">
					<c:forEach items="${branches}" var="branch">
						<c:if test="${prevInputBranch != branch.id}">
							<option value="${branch.id}">
							<c:out value="${branch.name}"/>
							</option>
						</c:if>
						<c:if test="${prevInputBranch == branch.id}">
							<option selected value="${branch.id}">
							<c:out value="${branch.name}"/>
							</option>
						</c:if>
					</c:forEach>
				</select>
				</div><br/>

				<div class="form">		役職			<br/>
				<select name="positionId" class="inputForm">
					<c:forEach items="${positions}" var="position">
						<c:if test="${prevInputPosition != position.id}">
							<option value="${position.id}">
							<c:out value="${position.name}"/>
							</option>
						</c:if>
						<c:if test="${prevInputPosition == position.id}">
							<option selected value="${position.id}">
							<c:out value="${position.name}"/>
							</option>
						</c:if>
					</c:forEach>
				</select>
				</div><br/>
			</c:if>

			<c:if test="${user.id == loginUser.id}">
				<input type=hidden name="branchId" value="${loginUser.branchId}">
				<input type=hidden name="positionId" value="${loginUser.positionId}">
			</c:if>

			<br/><div class="center"><input type="submit" class="btn-simple" value="変更する" ></div><br/>

		</form>

		<div class="copyright">Copyright(c)OchiaiShun</div>
	</div>
	</body>
</html>
