<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./CSS/style.css" rel="stylesheet" type="text/css">
		<title>目にやさしい掲示板</title>

<script type="text/javascript">

function deleteCheck(){
	if(window.confirm('削除しますがよろしいですか？\n削除した場合、内容の復元はできません')){
		window.alert('削除しました');
		return true;
	}
	else{
		window.alert('キャンセルしました');
		return false;
	}
}

</script>

	</head>
	<body>
		<div class="header">
			<ul class="table">
				<li class="name"><span class="hello">こんにちは　</span><c:out value="${loginUser.name}"/><span class="hello">　さん</span></li>
				<li><a href="./">トップ</a></li>
				<c:if test="${loginUser.positionId == 1 }">
					<li><a href="manageUser">		ユーザー管理</a></li>
				</c:if>
				<li><a href="newContribution">	新規投稿</a></li>
				<li><a href="logout">			ログアウト</a></li>
			</ul>
			<h1 class="h1">目にやさしい掲示板</h1>
		</div>

		<div class="main-contents">

			<c:if test="${loginTimeCounter >= 180 && empty errorMessages}">
				<div class="timer">
					大丈夫ですか？<br/>
					ログインしてから${loginTimeCounter}秒経っているようです
					目にやさしくしてください<br/>
					<a href="logout">ログアウト</a>
				</div>
			</c:if>

					<c:if test="${ not empty errorMessages }">
						<div class="errorMessages">
							<ul>
								<c:forEach items="${errorMessages}" var="message">
									<li><c:out value="${message}" />
								</c:forEach>
							</ul>
						</div>
						<c:remove var="errorMessages" scope="session"/>
					</c:if>

		<hr class="line" size="15" noshade>

					<form class="finder" action="./" method="get">
						投稿日検索<br/>
							<input type="date" id="dayStart" name="dayStart" value="${dayStart}" max="9999-12-31">から
							<input type="date" id="dayEnd" name="dayEnd" value="${dayEnd}"  max="9999-12-31">まで<br/><br/>
						カテゴリー検索（部分一致）<br/>
								<input name="findCategory" value="${findCategory}" id="findCategory">
						<input type="submit" class="btn-simple" value="検索する"> <input type="button" class="btn-simple" value="絞込みを解除" onClick="document.location='./';">
					</form>

		<hr class="line" size="15" noshade>

					<div class="contributions">

					<c:if test="${empty contributions}"><div class="timer">検索条件に当てはまる投稿がありませんでした</div></c:if>
						<c:forEach items="${contributions}" var="contribution">

							<div class="contribution">

								<div class="title">	タイトル：	<c:out value="${contribution.title}" /></div>
								<div class="category">	カテゴリー：	<c:out value="${contribution.category}" /></div>
								<span>
									<c:forEach var="s" items="${fn:split(contribution.text, '
																							')}">
	    								<div class="text"><c:out value="${s}"/></div>
									</c:forEach>
								</span>

								<div class="author">	投稿者	<c:out value="${contribution.name }"/></div>

								<form action="deleteContribution" method="post" onSubmit="return deleteCheck()">
									<input type="hidden" name="contribution_id" value="${contribution.id}">
									<c:if test="${contribution.userId == loginUser.id}">
										<input type="submit" class="deleteButton" value="投稿の削除">
									</c:if>
								</form>

								<span class="postDate">	投稿日	<fmt:formatDate value="${contribution.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></span><br/><br/>

								<details open>
								<summary class="closer">クリックしてコメント欄の開閉</summary>
								<div class="comment"><span class="mini-gray">以下にコメントを表示します</span></div>
									<div class="comment">
									<c:forEach items="${showComments}" var="comment">
										<c:if test="${comment.contributionId == contribution.id}">
											<span class="commentAuthor"><c:out value="${comment.name}"/></span>
											<span>
												<c:forEach var="s" items="${fn:split(comment.text, '
																									')}">
		    										<div class="text"><c:out value="${s}"/></div>
												</c:forEach>

											</span>

											<form action="deleteComment" method="post" onSubmit="return deleteCheck()">
												<input type="hidden" name="comment_id" value="${comment.id}">

												<c:if test="${comment.userId == loginUser.id}">
													<input type="submit" class="deleteButton" value="コメントの削除">
												</c:if>
											</form>
											<span class="postDate">	投稿日：<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss"/><br /></span>

										</c:if>
									</c:forEach>
									</div>
								</details>

								<div class="commentBox"><form action="newComment" method="post" >
									<br/>コメントする<br/>
									<textarea name="comment" maxlength="500" class="newComment" style="resize: none"></textarea>
									<input type="hidden" name="contribution_id" value="${contribution.id}"><br/>
									<input type="submit" class="btn-simple" value="コメントを送信">
								</form></div><br/>

							</div>
<hr class="line" size="15" noshade>
						</c:forEach>


					</div>

			<div class="copyright"> Copyright(c)OchiaiShun</div>
		</div>
	</body>
</html>
