package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Contribution;
import dao.ContributionDao;


public class ContributionService {

	public void register(Contribution contribution) {

		Connection connection = null;
		try {
			connection = getConnection();
			ContributionDao contributionDao = new ContributionDao();
			contributionDao.insert(connection, contribution);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Contribution> getContribution(String dayStart, String dayEnd, String findCategory) {

		Connection connection = null;
		String dStart;
		String dEnd;
		String fCategory;

		try {
			if(StringUtils.isBlank(dayStart)) {
				dStart = "2019-01-01 00:00:00";
			}else {
				dStart = dayStart + " 00:00:00";
			}

			if(StringUtils.isBlank(dayEnd)) {
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				dEnd = sdf.format(calendar.getTime());
			}else {
				dEnd = dayEnd + " 23:59:59";
			}
			if(findCategory == null) {
				fCategory = " ";
			}else {
				fCategory = findCategory;
			}


			connection = getConnection();

			ContributionDao contributionDao = new ContributionDao();
			List<Contribution> ret = contributionDao.getContributions(connection, dStart, dEnd ,fCategory);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}