package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Contribution;
import dao.ContributionDao;


public class DeleteContributionService {

	public void delete(Contribution contributionId) {

		Connection connection = null;
		try {
			connection = getConnection();
			ContributionDao contributionDao = new ContributionDao();
			contributionDao.delete(connection, contributionId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}