package beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;;
	private String text;
	private int contributionId;
	private int userId;
	private Date createdDate;
	private Date updatedDate;
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public String getText() {
		return text;
	}
	public int getContributionId() {
		return contributionId;
	}
	public int getUserId() {
		return userId;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setText(String text) {
		this.text = text;
	}
	public void setContributionId(int contributionId) {
		this.contributionId = contributionId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
