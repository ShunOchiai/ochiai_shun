package beans;

import java.io.Serializable;
import java.util.Date;

public class Position implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private Date createdDate;
	private Date updatedDate;
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}