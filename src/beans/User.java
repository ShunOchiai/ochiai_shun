package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String loginId;
	private String password;
	private String name;
	private int branchId;
	private int positionId;
	private int isDeleted;
	private Date createdDate;
	private Date updatedDate;
	private String branchName;
	private String positionName;

	public String getBranchName() {
		return branchName;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public int getId() {
		return id;
	}
	public String getLoginId() {
		return loginId;
	}
	public String getPassword() {
		return password;
	}
	public String getName() {
		return name;
	}
	public int getBranchId() {
		return branchId;
	}
	public int getPositionId() {
		return positionId;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


}