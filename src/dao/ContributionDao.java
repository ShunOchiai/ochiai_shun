package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Contribution;
import exception.SQLRuntimeException;

public class ContributionDao {

	public void insert(Connection connection, Contribution contribution) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO contributions ( ");
			sql.append("title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // title
			sql.append(", ?");// text
			sql.append(", ?"); // category
			sql.append(", ?"); // user_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, contribution.getTitle());
			ps.setString(2, contribution.getText());
			ps.setString(3, contribution.getCategory());
			ps.setInt(4, contribution.getUserId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public void delete(Connection connection, Contribution contributionId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM contributions WHERE id =");
			sql.append(" ? ");
			sql.append(";");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, contributionId.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Contribution> getContributions(Connection connection, String dayStart, String dayEnd, String findCategory) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("contributions.id as id, ");
			sql.append("contributions.title as title, ");
			sql.append("contributions.text as text, ");
			sql.append("contributions.category as category, ");
			sql.append("users.name as name, ");
			sql.append("contributions.user_id as user_id, ");
			sql.append("contributions.created_date as created_date, ");
			sql.append("contributions.updated_date as updated_date ");
			sql.append("FROM contributions ");
			sql.append("INNER JOIN users ");
			sql.append("ON contributions.user_id = users.id ");
			sql.append("WHERE contributions.created_date ");
			sql.append("BETWEEN '"+ dayStart + "' AND '"+ dayEnd +"' ");
				if(!StringUtils.isBlank(findCategory)) {
					sql.append("AND contributions.category LIKE '%"+ findCategory +"%'");
				}
			sql.append(" ORDER BY created_date DESC ;");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Contribution> ret = toContributionList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Contribution> toContributionList(ResultSet rs)
			throws SQLException {

		List<Contribution> ret = new ArrayList<Contribution>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				String name = rs.getString("name");
				int userId = rs.getInt("user_id");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				Contribution contribution = new Contribution();
				contribution.setId(id);
				contribution.setTitle(title);
				contribution.setText(text);
				contribution.setCategory(category);
				contribution.setName (name);
				contribution.setUserId(userId);
				contribution.setCreatedDate(createdDate);
				contribution.setUpdatedDate(updatedDate);
				ret.add(contribution);

			}
			return ret;
		} finally {
			close(rs);
		}
	}

}