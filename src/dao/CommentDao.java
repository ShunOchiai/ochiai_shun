package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append(" text");
			sql.append(", contribution_id");
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?");// text
			sql.append(", ?"); // contribution_id
			sql.append(", ?"); // user_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getContributionId());
			ps.setInt(3, comment.getUserId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public void delete(Connection connection, Comment commentId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE id =");
			sql.append(" ? ");
			sql.append(";");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, commentId.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public List<Comment> getComment(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id,");
			sql.append("comments.text as text, ");
			sql.append("comments.contribution_id as contribution_id, ");
			sql.append("users.name as name, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Comment> toCommentList(ResultSet rs)
			throws SQLException {

		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				int id =rs.getInt("id");
				String text = rs.getString("text");
				int contributionId = Integer.parseInt(rs.getString("contribution_id"));
				String name = rs.getString("name");
				int userId = rs.getInt("user_id");
				Timestamp createdDate = rs.getTimestamp("created_date");

				Comment comment = new Comment();
				comment.setId(id);
				comment.setText(text);
				comment.setContributionId(contributionId);
				comment.setName(name);
				comment.setUserId(userId);
				comment.setCreatedDate(createdDate);
				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}



}
