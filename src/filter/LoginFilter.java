package filter;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		ArrayList<String> messages = new ArrayList<String>() ;
		HttpSession session = ((HttpServletRequest) request).getSession();
		String path = ((HttpServletRequest) request).getServletPath();

		User user = (User) session.getAttribute("loginUser");

		
		if((user == null) && ((path.equals("/login.jsp") || path.equals("/login") || path.equals("/CSS/style.css") || path.equals("/CSS/login")) == true)){
			chain.doFilter(request, response);
			return;

		}else if(user == null) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);

			((HttpServletResponse) response).sendRedirect("login");
			return;

		}
		chain.doFilter(request, response);

	}

	@Override
	public void destroy() {

	}




}