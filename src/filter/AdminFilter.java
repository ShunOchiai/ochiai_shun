package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import beans.Contribution;
import beans.User;
import service.CommentService;
import service.ContributionService;

@WebFilter(urlPatterns = { "/manageUser" , "/editUser" , "/signup"})
public class AdminFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		ArrayList<String> messages = new ArrayList<String>() ;
		HttpSession session = ((HttpServletRequest) request).getSession();

		User user = (User) session.getAttribute("loginUser");

		if(user == null) {
			chain.doFilter(request, response);
			return;
		}

		if(user.getPositionId() != 1){
				messages.add("管理者にのみ許可された操作です");
				session.setAttribute("errorMessages", messages);

				List<Contribution> contributions = new ContributionService().getContribution(	request.getParameter("dayStart"),
																								request.getParameter("dayEnd"),
																								request.getParameter("FindCategory"));
				request.setAttribute("contributions", contributions);

				List<Comment> comments = new CommentService().getComment();
				request.setAttribute("showComments", comments);

				((HttpServletResponse) response).sendRedirect("./");
				return;
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {

	}




}