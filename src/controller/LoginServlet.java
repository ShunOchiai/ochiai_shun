package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");

		if(user != null) {
			response.sendRedirect("./");
			return;
		}

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		LoginService loginService = new LoginService();
		User user = loginService.login(loginId, password);

		HttpSession session = request.getSession();

		if(StringUtils.isBlank(loginId) || StringUtils.isBlank(password)) {
			messages.add("ログインIDかパスワードが入力されていません。");
			session.setAttribute("errorMessages", messages);
			request.setAttribute("loginId", request.getParameter("loginId"));
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		if((user != null) && (user.getIsDeleted() == 1)) {
			messages.add("ログインに失敗しました。");//内部的には失敗ではなく、アカウント凍結
			session.setAttribute("errorMessages", messages);
			request.setAttribute("loginId", request.getParameter("loginId"));
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;

		}else if (user != null) {
			Date loginTime;
			Calendar calendar = Calendar.getInstance();
			loginTime = calendar.getTime();

			session.setAttribute("loginTime", loginTime);
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
			return;

		}else {
			messages.add("ログインに失敗しました。");
			session.setAttribute("errorMessages", messages);
			request.setAttribute("loginId", request.getParameter("loginId"));
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}




	}

}