package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.Contribution;
import service.CommentService;
import service.ContributionService;
import service.DeleteContributionService;

@WebServlet(urlPatterns = { "/deleteContribution" })
public class DeleteContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		response.sendRedirect("./");
	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		Contribution deleteContribution = new Contribution();
		deleteContribution.setId(Integer.parseInt(request.getParameter("contribution_id")));
		new DeleteContributionService().delete(deleteContribution);


		List<Contribution> contributions = new ContributionService().getContribution(	request.getParameter("dayStart"),
		request.getParameter("dayEnd"),
		request.getParameter("FindCategory"));
		request.setAttribute("contributions", contributions);

		List<Comment> comments = new CommentService().getComment();
		request.setAttribute("showComments", comments);

		request.setAttribute("dayStart", request.getParameter("dayStart") );
		request.setAttribute("dayEnd", request.getParameter("dayEnd") );

		response.sendRedirect("./");
	}
}