package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Contribution;
import beans.User;
import service.CommentService;
import service.ContributionService;

@WebServlet(urlPatterns = { "/newContribution" })
public class NewContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("newContribution.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Contribution contribution = new Contribution();
			contribution.setTitle(request.getParameter("title"));
			contribution.setText(request.getParameter("contribution"));
			contribution.setCategory(request.getParameter("category"));
			contribution.setUserId(user.getId());

			new ContributionService().register(contribution);

			List<Contribution> contributions = new ContributionService().getContribution(	request.getParameter("dayStart"),
			request.getParameter("dayEnd"),
			request.getParameter("FindCategory"));
			request.setAttribute("contributions", contributions);

			List<Comment> comments = new CommentService().getComment();
			request.setAttribute("showComments", comments);

			request.setAttribute("dayStart", request.getParameter("dayStart") );
			request.setAttribute("dayEnd", request.getParameter("dayEnd") );

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);

			request.setAttribute("title", request.getParameter("title"));
			request.setAttribute("contribution", request.getParameter("contribution"));
			request.setAttribute("category", request.getParameter("category"));

			request.getRequestDispatcher("newContribution.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String contribution = request.getParameter("contribution");
		String category = request.getParameter("category");

		if (StringUtils.isBlank(title) == true) {
			messages.add("タイトルを入力してください");
		}else if (30 < title.length()) {
		messages.add("タイトルは30文字以下で入力してください");
		}

		if (StringUtils.isBlank(contribution) == true) {
			messages.add("本文を入力してください");
		}else if (1000 < contribution.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}

		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}else if(10 < category.length()) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}



		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}