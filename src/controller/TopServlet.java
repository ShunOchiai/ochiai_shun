
package controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import beans.Contribution;
import service.CommentService;
import service.ContributionService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<Contribution> contributions = new ContributionService().getContribution(	request.getParameter("dayStart"),
																						request.getParameter("dayEnd"),
																						request.getParameter("findCategory"));
		request.setAttribute("contributions", contributions);

		List<Comment> comments = new CommentService().getComment();
		request.setAttribute("showComments", comments);

		request.setAttribute("dayStart", request.getParameter("dayStart") );
		request.setAttribute("dayEnd", request.getParameter("dayEnd") );
		request.setAttribute("findCategory", request.getParameter("findCategory") );

		Date now;
		Calendar calendar = Calendar.getInstance();
		now = calendar.getTime();

		long nowTime = now.getTime();
		long loginTime = ((Date) session.getAttribute("loginTime")).getTime();

		long loginTimeCounter = ( nowTime - loginTime  ) / (1000);
		session.setAttribute("loginTimeCounter", loginTimeCounter);

		request.getRequestDispatcher("top.jsp").forward(request, response);
	}
}