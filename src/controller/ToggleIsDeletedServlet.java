package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/toggleIsDeleted" })
public class ToggleIsDeletedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("isDeleted")));
		new UserService().isDeleted(user);

		response.sendRedirect("manageUser");
		//request.getRequestDispatcher("/manageUser").forward(request, response);
	}
}