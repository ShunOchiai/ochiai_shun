package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branches = new BranchService().getBranches();
		request.setAttribute("branches", branches);

		List<Position> positions = new PositionService().getPositions();
		request.setAttribute("positions", positions);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();

		//HttpSession session = request.getSession();

		if (isValid(request, messages) == true) {
			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setPositionId(Integer.parseInt(request.getParameter("positionId")));
			user.setIsDeleted(0);

			new UserService().register(user);

			response.sendRedirect("manageUser");
		} else {
			request.setAttribute("loginId", request.getParameter("loginId"));
			request.setAttribute("name", request.getParameter("name"));
			request.setAttribute("prevInputBranch",request.getParameter("branchId"));
			request.setAttribute("prevInputPosition", request.getParameter("positionId"));

			List<Branch> branches = new BranchService().getBranches();
			request.setAttribute("branches", branches);

			List<Position> positions = new PositionService().getPositions();
			request.setAttribute("positions", positions);

			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		User user = new User();
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int positionId = Integer.parseInt(request.getParameter("positionId"));

		user.setLoginId(loginId);

		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}else if (!(loginId.matches("^[a-zA-Z0-9]{6,20}$"))) {
			messages.add("ログインIDは半角英数字のみを使用し、6～20文字にしてください");
		}else if (new UserService().checkUser(user) != null ) {
			messages.add("そのログインIDは既に使用されています");
		}

		if(StringUtils.isBlank(password)){
			messages.add("パスワードを入力してください");
		}else if(!password.matches("^[!-~]{6,20}$")){
			messages.add("パスワードは半角英数字と記号のみを使用し、6～20文字にしてください");
		}else if(!password.equals(checkPassword)) {
			messages.add("確認用パスワードが違います");
		}


		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}else if (name.length() >= 10) {
			messages.add("名前は10文字以内にしてください");
		}

		if (((branchId == 1) && (positionId != 1)) && ((branchId == 1) && (positionId != 2 ))){
			messages.add("本社所属の場合、役職は「総務人事担当」か「情報管理担当」を選んでください");
		}

		if (((branchId != 1) && (positionId == 1)) || ((branchId != 1) && (positionId == 2))) {
			messages.add("支店所属の場合、役職は「支店長」か「社員」を選んでください");
		}


		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}