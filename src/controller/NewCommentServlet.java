package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Contribution;
import beans.User;
import service.CommentService;
import service.ContributionService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		response.sendRedirect("./");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();
			comment.setText(request.getParameter("comment"));
			comment.setContributionId(Integer.parseInt(request.getParameter("contribution_id")));
			comment.setUserId(user.getId());

			new CommentService().register(comment);

			} else {
			session.setAttribute("errorMessages", messages);
			}

		List<Contribution> contributions = new ContributionService().getContribution(	request.getParameter("dayStart"),
																						request.getParameter("dayEnd"),
																						request.getParameter("FindCategory"));
		request.setAttribute("contributions", contributions);

		List<Comment> comments = new CommentService().getComment();
		request.setAttribute("showComments", comments);

		request.setAttribute("dayStart", request.getParameter("dayStart") );
		request.setAttribute("dayEnd", request.getParameter("dayEnd") );

		response.sendRedirect("./");

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String comment = request.getParameter("comment");

		if (StringUtils.isBlank(comment) == true) {
			messages.add("コメントを入力してください");
		}else if (500 < comment.length()) {
			messages.add("コメントは500文字以下で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}